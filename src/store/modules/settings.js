// import variables from '@/styles/element-variables.scss'
import defaultSettings from '@/settings'

const { rippleActive, inputStyle, layoutMode, darkMenu, profileMode, layout, theme, scale, tagsView} = defaultSettings

const state = {
  rippleActive: localStorage.getItem('rippleActive') ? localStorage.getItem('rippleActive')==='true' : rippleActive,
  inputStyle: localStorage.getItem('inputStyle') || inputStyle,
  layoutMode: localStorage.getItem('layoutMode') || layoutMode,
  darkMenu: localStorage.getItem('darkMenu') ?  localStorage.getItem('darkMenu')==='true' : darkMenu,
  profileMode: localStorage.getItem('profileMode') || profileMode,
  layout: localStorage.getItem('layout') || layout,
  theme: localStorage.getItem('theme') || theme,
  scale: localStorage.getItem('scale') || scale,
  tagsView: localStorage.getItem('tagsView') ?  localStorage.getItem('tagsView')==='true' : tagsView
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
      localStorage.setItem(key, value)
    }
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

